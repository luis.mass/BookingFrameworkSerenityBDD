package stepDefinitions;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import lombok.extern.slf4j.Slf4j;
import net.thucydides.core.annotations.Steps;
import steps.UserBooking;

import static org.hamcrest.MatcherAssert.assertThat;

@Slf4j
public class UserStepDefinitions {
    String actor;

    @Steps(shared = true)
    UserBooking user;

    @Dado("^que el (.*) entra a la sección de alojamientos de Booking$")
    public void go_to_booking_site(String username) {
        user.isCalled(username);
        user.go_to_booking_site();
    }

    @Cuando("^El (.*) realiza la búsqueda de: (.*)$")
    public void search_by_key_word(String username, String keyWord) {
        user.isCalled(username);
        user.search_by_key_word(keyWord);
    }

    @Entonces("^El (.*) solo debería poder ver los resultados relacionados con la palabra: (.*)$")
    public void should_look_results_with_the_word(String username, String keyWordWaited) {
        user.isCalled(username);
        user.should_look_results_with_the_word(keyWordWaited);
    }
}

