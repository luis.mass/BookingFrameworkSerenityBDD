package steps;

import lombok.extern.slf4j.Slf4j;
import net.serenitybdd.core.steps.ScenarioActor;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;
import pages.StayInputPage;
import pages.StaysResultPage;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;


@Slf4j
public class UserBooking extends ScenarioActor {

    String actor;

    @Steps(shared=true)
    StayInputPage stayInputPage;

    @Steps(shared=true)
    StaysResultPage staysResultPage;

    @Step("#actor go to main page of Booking site")
    public void go_to_booking_site() {
        stayInputPage.setDefaultBaseUrl("https://www.booking.com/");
        stayInputPage.open();
    }

    @Step("#actor run searching of :{0}")
    public void search_by_key_word(String keyWord) {
        stayInputPage.typeKeyWord(keyWord);
        stayInputPage.runSearch();
    }

    @Step("#actor should look results that contains the word :{0}")
    public void should_look_results_with_the_word(String keyWordWaited) {
        List<String> results = staysResultPage.getResults();
        results.replaceAll(String::toLowerCase);
        for (String x :results){
            log.info(x);
        }
        assertThat(results, Matchers.everyItem(Matchers.containsString(keyWordWaited.toLowerCase())));
    }
}
