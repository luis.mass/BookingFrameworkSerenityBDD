package pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://www.booking.com/")
public class StayInputPage extends PageObject {

    @FindBy(css = "#ss")
    public WebElementFacade destination;

    @FindBy(xpath = "//span[contains(text(),'Search')]")
    public WebElementFacade buttonSearch;

    public void typeKeyWord(String keyWord) {
        destination.type(keyWord);
    }

    public void runSearch() {
        buttonSearch.click();
    }

}
