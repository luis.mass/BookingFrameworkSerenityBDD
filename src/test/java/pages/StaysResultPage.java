package pages;

import net.serenitybdd.core.pages.PageObject;
import java.util.List;
import java.util.stream.Collectors;


public class StaysResultPage extends PageObject {

    public static final String TITLE_RESULT = "//div/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/a[1]/span[1]/span[1]";

    public List<String> getResults() {
        return findAll(TITLE_RESULT)
                .stream()
                .map(element -> element.getAttribute("textContent")) //convert to string
                .collect(Collectors.toList()); // save in a list
    }

}
