# language: es

  @Run
  Característica: Búsqueda de Alojamiento

    Este feature se trata de realizar búsquedas de alojamiento en el sitio de Booking y revisar los resultados
    obtenidos de esas búsuqedas.

    @Stays
    Esquema del escenario: Buscar resultados relevantes
      Dado que el Tester entra a la sección de alojamientos de Booking
      Cuando El Tester realiza la búsqueda de: <ciudad>
      Entonces El Tester solo debería poder ver los resultados relacionados con la palabra: <ciudad>

      Ejemplos:
      |ciudad|
      |Cartagena|
      |Bogotá|
      |San Andrés|
